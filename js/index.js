var app = {
    _listOfFiles: [
        '1-bg.jpg',
        '10-bg.jpg',
        '10-click-1.jpg',
        '10-click-10.jpg',
        '10-click-2.jpg',
        '10-click-3.jpg',
        '10-click-4.jpg',
        '10-click-5.jpg',
        '10-click-6.jpg',
        '10-click-7.jpg',
        '10-click-8.jpg',
        '10-click-9.jpg',
        '10-click-default.jpg',
        '11-bg.jpg',
        '12-bg.jpg',
        '12-click-1.jpg',
        '12-click-2.jpg',
        '12-click-3.jpg',
        '12-click-default.jpg',
        '13-bg.jpg',
        '13-click-1.jpg',
        '13-click-default.jpg',
        '14-bg.jpg',
        '14-click-1.jpg',
        '14-click-2.jpg',
        '14-click-default.jpg',
        '15-bg.jpg',
        '15-click-1.jpg',
        '15-click-default.jpg',
        '16-bg.jpg',
        '17-bg.jpg',
        '17-click-1.jpg',
        '17-click-2.jpg',
        '17-click-3.jpg',
        '17-click-4.jpg',
        '17-click-default.jpg',
        '18-bg.jpg',
        '19-bg.jpg',
        '2-bg.jpg',
        '2-click-1.jpg',
        '2-click-2.jpg',
        '2-click-3.jpg',
        '2-click-4.jpg',
        '2-click-5.jpg',
        '2-click-6.jpg',
        '2-click-7.jpg',
        '2-click-8.jpg',
        '2-click-default.jpg',
        '20-bg.jpg',
        '20-click-1.jpg',
        '20-click-2.jpg',
        '20-click-3.jpg',
        '20-click-4.jpg',
        '20-click-5.jpg',
        '21-bg.jpg',
        '21-click-1.jpg',
        '21-click-2.jpg',
        '21-click-3.jpg',
        '21-click-4.jpg',
        '21-click-5.jpg',
        '22-bg.jpg',
        '22-click-1.jpg',
        '22-click-2.jpg',
        '22-click-3.jpg',
        '22-click-4.jpg',
        '22-click-5.jpg',
        '23-bg.jpg',
        '24-bg.jpg',
        '24-click-1.jpg',
        '24-click-2.jpg',
        '24-click-3.jpg',
        '24-click-4.jpg',
        '24-click-default.jpg',
        '25-bg.jpg',
        '25-click-1.jpg',
        '25-click-2.jpg',
        '25-click-3.jpg',
        '25-click-default.jpg',
        '26-bg.jpg',
        '26-click-1.jpg',
        '26-click-2.jpg',
        '26-click-default.jpg',
        '27-bg.jpg',
        '27-click-1.jpg',
        '27-click-2.jpg',
        '27-click-3.jpg',
        '27-click-4.jpg',
        '27-click-5.jpg',
        '28-bg.jpg',
        '29-bg.jpg',
        '29-slide1-2.jpg',
        '29-slide1-3.jpg',
        '29-slide1-4.jpg',
        '29-slide2-1.jpg',
        '29-slide2-2.jpg',
        '29-slide2-3.jpg',
        '29-slide2-4.jpg',
        '3-bg.jpg',
        '30-bg.jpg',
        '30-click-1.jpg',
        '30-click-2.jpg',
        '30-click-3.jpg',
        '31-bg.jpg',
        '31-slide1.jpg',
        '31-slide2.jpg',
        '31-slide3.jpg',
        '31-slide4.jpg',
        '32-bg.jpg',
        '33-bg.jpg',
        '33-click-1.jpg',
        '33-click-2.jpg',
        '33-click-3.jpg',
        '33-click-4.jpg',
        '33-click-5.jpg',
        '34-bg.jpg',
        '34-click-1.jpg',
        '34-click-2.jpg',
        '34-click-3.jpg',
        '34-click-4.jpg',
        '34-click-5.jpg',
        '34-click-6.jpg',
        '34-click-7.jpg',
        '34-click-default.jpg',
        '35-bg.jpg',
        '35-click-1.jpg',
        '35-click-2.jpg',
        '35-click-3.jpg',
        '35-click-4.jpg',
        '36-bg.jpg',
        '4-bg.jpg',
        '4-slide1.jpg',
        '4-slide2.jpg',
        '5-bg.jpg',
        '6-bg.jpg',
        '7-bg.jpg',
        '7-slide1.jpg',
        '7-slide10.jpg',
        '7-slide11.jpg',
        '7-slide12.jpg',
        '7-slide13.jpg',
        '7-slide2.jpg',
        '7-slide3.jpg',
        '7-slide4.jpg',
        '7-slide5.jpg',
        '7-slide6.jpg',
        '7-slide7.jpg',
        '7-slide8.jpg',
        '7-slide9.jpg',
        '7-video.mp4',
        '8-bg.jpg',
        '9-bg.jpg',
        'menu-links.jpg',
        'menu-slide-1.jpg',
        'menu-slide-2.jpg',
        'menu-slide-3.jpg',
        'menu-slide-4.jpg',
        'menu-slide-5.jpg',
        'menu-slide-6.jpg',
        'menu-slide-7.jpg'
    ],

    _currentFileKey: 0,
    _fileUrl: 'http://www.freeda.fr/fdj-tutobook/',
    _fileSystem: false,

    // Application Constructor
    initialize: function() {
        app.bindEvents();
    },

    bindEvents: function() {
        document.addEventListener('deviceready', app.onDeviceReady, false);
    },

    onDeviceReady: function() {
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, app.gotFS, app.failFS);
    },

    failFS: function(error) {

    },

    gotFS: function(fileSystem) {
        app._fileSystem = fileSystem;

        app.startLoadingFiles();
    },

    startLoadingFiles: function() {
        if (app._currentFileKey < app._listOfFiles.length) {
            app._fileSystem.root.getFile(app._listOfFiles[app._currentFileKey], null, app.gotFileEntry, app.fail);
        }
        else {
            app.weHaveAllTheFiles();
        }
    },

    gotFileEntry: function(fileEntry) {
        app._currentFileKey++;

        var percentTotal = Math.round(app._currentFileKey * 100 / app._listOfFiles.length);
        $('#totalProgressBar').css('width', percentTotal + '%');
        $('#totalProgressText').html(percentTotal + '%');

        app.startLoadingFiles();
    },

    fail: function(evt) {
        var fileTransfer = new FileTransfer();

        fileTransfer.onprogress = function(progressEvent) {
            if (progressEvent.lengthComputable) {
                var percent = Math.round(progressEvent.loaded * 100 / progressEvent.total);
                $('#singleProgressBar').css('width', percent + '%');
                $('#singleProgressText').html(percent + '%');
                $('#singleProgressTextDetails').html(Math.round(progressEvent.loaded / 1000) + 'ko / ' + Math.round(progressEvent.total / 1000) + 'ko');
            }
            else {
                $('#singleProgressBar').css('width', '0%');
                $('#singleProgressText').html('Chargement...');
                $('#singleProgressTextDetails').html();
            }
        };

        fileTransfer.download(
            app._fileUrl + app._listOfFiles[app._currentFileKey],
            app._fileSystem.root.fullPath + '/' + app._listOfFiles[app._currentFileKey],
            function(theFile) {
                app.gotFileEntry();
            },
            function(error) {
            }
        );
    },

    weHaveAllTheFiles: function() {
		window.location.replace('app/index.html');
    }
};

$(document).ready(function() {
	app.initialize();
});
