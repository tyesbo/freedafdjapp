var speed=500;
var currentItem = 1;
var numberOfItems;
var menu = true;

var $item;
var $holder;
var $app;

function swipeStatus(event, phase, direction, distance) {
	if( phase=="move" && (direction=="left" || direction=="right") ) {
        replaceApp();

		var duration=0;
        var width = (currentItem - 1) * getBrowserWidth();

		if (direction == "left") {
            scrollHolder(width + distance, duration);
        }

		else if (direction == "right") {
            scrollHolder(width - distance, duration);
        }
	}
    else if( phase=="move" && (direction=="down" || direction=="up") ) {
        replaceHolder();

		var duration=0;
        var height = getBrowserHeight();

		if (direction == "up") {
            if (menu == true) {
                scrollApp(distance, duration);
            }
            else {
                scrollApp(height + distance, duration);
            }
        }

		else if (direction == "down") {
            if (menu == true) {
                scrollApp(0 - distance, duration);
            }
            else {
                scrollApp(height - distance, duration);
            }
        }
    }

	else if ( phase == "end"  && distance <= 75 && (direction=="left" || direction=="right") ) {
        replaceHolder();
	}
	else if ( phase == "end"  && distance <= 75 && (direction=="up" || direction=="down") ) {
        replaceApp();
	}

	else if ( phase =="end" )
	{
		if (direction == "right") {
			previousImage();
        }
		else if (direction == "left") {
			nextImage();
        }
        else if (direction == "down") {
            showMenu();
        }
        else if (direction == "up") {
            hideMenu();
        }
	}
}

function gotoPage(id) {
    currentItem = id;
    hideMenu();
    replaceHolder();
}

function showMenu() {
    menu = true;
    scrollApp('0', speed);
    $('#menu-slideshow').data('nivoslider').start();
}

function hideMenu() {
    menu = false;
    scrollApp(getBrowserHeight(), speed);
    $('#menu-slideshow').data('nivoslider').stop();
}

function previousImage() {
    if (currentItem > 1) {
        currentItem--;
    }

    replaceHolder();
    stopAllNivoAndActivateCurrentOnly()
}

function nextImage() {
    if (currentItem < numberOfItems) {
        currentItem++;
    }

    replaceHolder();
    stopAllNivoAndActivateCurrentOnly()
}

function stopAllNivoAndActivateCurrentOnly() {
    $('.itemslide').each(function () {
        $(this).data('nivoslider').stop();
    });

    var $its = $('.item' + currentItem + ' .itemslide');
    if ($its.length > 0) {
        $its.each(function () {
            $(this).data('nivoslider').start();
        });
    }
}

function replaceHolder() {
	scrollHolder( getBrowserWidth() * (currentItem - 1), speed);
}

function replaceApp() {
    if (menu == true) {
        showMenu();
    }
    else {
        hideMenu();
    }
}

// Items scroll
function scrollHolder(distance, duration) {
	$holder.css("-webkit-transition-duration", (duration/1000).toFixed(1) + "s");
	$holder.css("-moz-transition-duration", (duration/1000).toFixed(1) + "s");
	$holder.css("-ms-transition-duration", (duration/1000).toFixed(1) + "s");
	$holder.css("transition-duration", (duration/1000).toFixed(1) + "s");

	var value = (distance<0 ? "" : "-") + Math.abs(distance).toString();
	$holder.css("-webkit-transform", "translate3d("+value +"px,0px,0px)");
    $holder.css("-moz-transform", "translate3d("+value +"px,0px,0px)");
    $holder.css("-ms-transform", "translate3d("+value +"px,0px,0px)");
    $holder.css("transform", "translate3d("+value +"px,0px,0px)");
}

// Menu scroll
function scrollApp(distance, duration) {
    $app.css("-webkit-transition-duration", (duration/1000).toFixed(1) + "s");
    $app.css("-moz-transition-duration", (duration/1000).toFixed(1) + "s");
    $app.css("-ms-transition-duration", (duration/1000).toFixed(1) + "s");
    $app.css("transition-duration", (duration/1000).toFixed(1) + "s");

	var value = (distance<0 ? "" : "-") + Math.abs(distance).toString();
	$app.css("-webkit-transform", "translate3d(0px," + value + "px,0px)");
	$app.css("-moz-transform", "translate3d(0px," + value + "px,0px)");
	$app.css("-ms-transform", "translate3d(0px," + value + "px,0px)");
	$app.css("transform", "translate3d(0px," + value + "px,0px)");
}

function getBrowserWidth() {
    return 1024;
    //return $(window).width();
}

function getBrowserHeight() {
    return 768;
    //return $(window).height();
}



$(document).ready(function () {
    $('.app').swipe({
		triggerOnTouchEnd : true,
		swipeStatus : swipeStatus,
		allowPageScroll:"none",
		threshold:75,
        excludedElements: 'button, input, select, textarea, .noSwipe'
    });

    $item = $('.item');
    numberOfItems = $item.length;

    $holder = $('.holderInner');
    $app = $('.app');

    $('.menu a, .item a.innerlink').each(function() {
        var $a = $(this);
        $a.click(function(e) {
            e.preventDefault();
            gotoPage(parseInt($a.attr('data-item')));

            return false;
        });
    });

    $("#menu-slideshow").nivoSlider({
        effect:"fade",
        slices:1,
        boxCols:1,
        boxRows:1,
        animSpeed:500,
        pauseTime:4000,
        startSlide:0,
        directionNav:false,
        controlNav:false,
        controlNavThumbs:false,
        pauseOnHover:false,
        manualAdvance:false
    });

    $(".itemslide").nivoSlider({
        effect:"fade",
        slices:1,
        boxCols:1,
        boxRows:1,
        animSpeed:500,
        pauseTime:3000,
        startSlide:0,
        directionNav:false,
        controlNav:false,
        controlNavThumbs:false,
        pauseOnHover:false,
        manualAdvance:false
    });

    // Disable the ios bouncing effect on touch scroll.
    // We created the effect in JS.
    document.ontouchmove = function(event) {
        event.preventDefault();
    }

    $('.links-fade-images a').each(function () {
        var $this = $(this);
        var img = $this.attr('data-image');
        var $container = $('.container', $this.parent());

        $this.click(function (e) {
            e.preventDefault();

            $('img', $container).css('z-index', '10');
            $('img', $container).addClass('old');
            $container.append('<img src="../../Documents/' + img + '" />');
            $('img.old', $container).fadeOut(400, function() {
                $(this).remove();
            });

            return false;
        });
    });

    $('.holder .controls a#previous').click(function(event) {
        event.preventDefault();
        previousImage();
    });

    $('.holder .controls a#next').click(function(event) {
        event.preventDefault();
        nextImage();
    });

    $('.holder .controls a#menuclick').click(function(event) {
        event.preventDefault();
        showMenu();
    });
});
